package main

import (
	"fmt"

	"gopkg.in/gin-gonic/gin.v1"

	"github.com/jinzhu/gorm"
	"github.com/wangzitian0/golang-gin-starter-kit/entities"
	"github.com/wangzitian0/golang-gin-starter-kit/common"
	"github.com/wangzitian0/golang-gin-starter-kit/users"
)

func Migrate(db *gorm.DB) {
	users.AutoMigrate()
	db.AutoMigrate(&entities.entityModel{})
	db.AutoMigrate(&entities.TagModel{})
	db.AutoMigrate(&entities.FavoriteModel{})
	db.AutoMigrate(&entities.entityUserModel{})
	db.AutoMigrate(&entities.CommentModel{})
}

func main() {

	db := common.Init()
	Migrate(db)
	defer db.Close()

	r := gin.Default()

	v1 := r.Group("/api")
	users.UsersRegister(v1.Group("/users"))
	v1.Use(users.AuthMiddleware(false))
	entities.EntitiesAnonymousRegister(v1.Group("/entities"))
	entities.TagsAnonymousRegister(v1.Group("/tags"))

	v1.Use(users.AuthMiddleware(true))
	users.UserRegister(v1.Group("/user"))
	users.ProfileRegister(v1.Group("/profiles"))

	entities.EntitiesRegister(v1.Group("/entities"))

	testAuth := r.Group("/api/ping")

	testAuth.GET("/", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})

	// test 1 to 1
	tx1 := db.Begin()
	userA := users.UserModel{
		Username: "AAAAAAAAAAAAAAAA",
		Email:    "aaaa@g.cn",
		Bio:      "hehddeda",
		Image:    nil,
	}
	tx1.Save(&userA)
	tx1.Commit()
	fmt.Println(userA)

	//db.Save(&entityUserModel{
	//    UserModelID:userA.ID,
	//})
	//var userAA entityUserModel
	//db.Where(&entityUserModel{
	//    UserModelID:userA.ID,
	//}).First(&userAA)
	//fmt.Println(userAA)

	r.Run() // listen and serve on 0.0.0.0:8080
}
