package entities

import (
	"errors"
	"github.com/wangzitian0/golang-gin-starter-kit/common"
	"github.com/wangzitian0/golang-gin-starter-kit/users"
	"gopkg.in/gin-gonic/gin.v1"
	"net/http"
	"strconv"
)

func EntitiesRegister(router *gin.RouterGroup) {
	router.POST("/", entityCreate)
	router.PUT("/:slug", entityUpdate)
	router.DELETE("/:slug", entityDelete)
	router.POST("/:slug/favorite", entityFavorite)
	router.DELETE("/:slug/favorite", entityUnfavorite)
	router.POST("/:slug/comments", entityCommentCreate)
	router.DELETE("/:slug/comments/:id", entityCommentDelete)
}

func EntitiesAnonymousRegister(router *gin.RouterGroup) {
	router.GET("/", entityList)
	router.GET("/:slug", entityRetrieve)
	router.GET("/:slug/comments", entityCommentList)
}

func TagsAnonymousRegister(router *gin.RouterGroup) {
	router.GET("/", TagList)
}

func entityCreate(c *gin.Context) {
	entityModelValidator := NewentityModelValidator()
	if err := entityModelValidator.Bind(c); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewValidatorError(err))
		return
	}
	//fmt.Println(entityModelValidator.entityModel.Author.UserModel)

	if err := SaveOne(&entityModelValidator.entityModel); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewError("database", err))
		return
	}
	serializer := entitieserializer{c, entityModelValidator.entityModel}
	c.JSON(http.StatusCreated, gin.H{"entity": serializer.Response()})
}

func entityList(c *gin.Context) {
	//condition := entityModel{}
	tag := c.Query("tag")
	author := c.Query("author")
	favorited := c.Query("favorited")
	limit := c.Query("limit")
	offset := c.Query("offset")
	entityModels, modelCount, err := FindManyentity(tag, author, limit, offset, favorited)
	if err != nil {
		c.JSON(http.StatusNotFound, common.NewError("entities", errors.New("Invalid param")))
		return
	}
	serializer := EntitiesSerializer{c, entityModels}
	c.JSON(http.StatusOK, gin.H{"entities": serializer.Response(), "entitiesCount": modelCount})
}

func entityFeed(c *gin.Context) {
	limit := c.Query("limit")
	offset := c.Query("offset")
	myUserModel := c.MustGet("my_user_model").(users.UserModel)
	if myUserModel.ID == 0 {
		c.AbortWithError(http.StatusUnauthorized, errors.New("{error : \"Require auth!\"}"))
		return
	}
	entityUserModel := GetentityUserModel(myUserModel)
	entityModels, modelCount, err := entityUserModel.GetentityFeed(limit, offset)
	if err != nil {
		c.JSON(http.StatusNotFound, common.NewError("entities", errors.New("Invalid param")))
		return
	}
	serializer := EntitiesSerializer{c, entityModels}
	c.JSON(http.StatusOK, gin.H{"entities": serializer.Response(), "entitiesCount": modelCount})
}

func entityRetrieve(c *gin.Context) {
	slug := c.Param("slug")
	if slug == "feed" {
		entityFeed(c)
		return
	}
	entityModel, err := FindOneentity(&entityModel{Slug: slug})
	if err != nil {
		c.JSON(http.StatusNotFound, common.NewError("entities", errors.New("Invalid slug")))
		return
	}
	serializer := entitieserializer{c, entityModel}
	c.JSON(http.StatusOK, gin.H{"entity": serializer.Response()})
}

func entityUpdate(c *gin.Context) {
	slug := c.Param("slug")
	entityModel, err := FindOneentity(&entityModel{Slug: slug})
	if err != nil {
		c.JSON(http.StatusNotFound, common.NewError("entities", errors.New("Invalid slug")))
		return
	}
	entityModelValidator := NewentityModelValidatorFillWith(entityModel)
	if err := entityModelValidator.Bind(c); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewValidatorError(err))
		return
	}

	entityModelValidator.entityModel.ID = entityModel.ID
	if err := entityModel.Update(entityModelValidator.entityModel); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewError("database", err))
		return
	}
	serializer := entitieserializer{c, entityModel}
	c.JSON(http.StatusOK, gin.H{"entity": serializer.Response()})
}

func entityDelete(c *gin.Context) {
	slug := c.Param("slug")
	err := DeleteentityModel(&entityModel{Slug: slug})
	if err != nil {
		c.JSON(http.StatusNotFound, common.NewError("entities", errors.New("Invalid slug")))
		return
	}
	c.JSON(http.StatusOK, gin.H{"entity": "Delete success"})
}

func entityFavorite(c *gin.Context) {
	slug := c.Param("slug")
	entityModel, err := FindOneentity(&entityModel{Slug: slug})
	if err != nil {
		c.JSON(http.StatusNotFound, common.NewError("entities", errors.New("Invalid slug")))
		return
	}
	myUserModel := c.MustGet("my_user_model").(users.UserModel)
	err = entityModel.favoriteBy(GetentityUserModel(myUserModel))
	serializer := entitieserializer{c, entityModel}
	c.JSON(http.StatusOK, gin.H{"entity": serializer.Response()})
}

func entityUnfavorite(c *gin.Context) {
	slug := c.Param("slug")
	entityModel, err := FindOneentity(&entityModel{Slug: slug})
	if err != nil {
		c.JSON(http.StatusNotFound, common.NewError("entities", errors.New("Invalid slug")))
		return
	}
	myUserModel := c.MustGet("my_user_model").(users.UserModel)
	err = entityModel.unFavoriteBy(GetentityUserModel(myUserModel))
	serializer := entitieserializer{c, entityModel}
	c.JSON(http.StatusOK, gin.H{"entity": serializer.Response()})
}

func entityCommentCreate(c *gin.Context) {
	slug := c.Param("slug")
	entityModel, err := FindOneentity(&entityModel{Slug: slug})
	if err != nil {
		c.JSON(http.StatusNotFound, common.NewError("comment", errors.New("Invalid slug")))
		return
	}
	commentModelValidator := NewCommentModelValidator()
	if err := commentModelValidator.Bind(c); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewValidatorError(err))
		return
	}
	commentModelValidator.commentModel.entity = entityModel

	if err := SaveOne(&commentModelValidator.commentModel); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewError("database", err))
		return
	}
	serializer := CommentSerializer{c, commentModelValidator.commentModel}
	c.JSON(http.StatusCreated, gin.H{"comment": serializer.Response()})
}

func entityCommentDelete(c *gin.Context) {
	id64, err := strconv.ParseUint(c.Param("id"), 10, 32)
	id := uint(id64)
	if err != nil {
		c.JSON(http.StatusNotFound, common.NewError("comment", errors.New("Invalid id")))
		return
	}
	err = DeleteCommentModel([]uint{id})
	if err != nil {
		c.JSON(http.StatusNotFound, common.NewError("comment", errors.New("Invalid id")))
		return
	}
	c.JSON(http.StatusOK, gin.H{"comment": "Delete success"})
}

func entityCommentList(c *gin.Context) {
	slug := c.Param("slug")
	entityModel, err := FindOneentity(&entityModel{Slug: slug})
	if err != nil {
		c.JSON(http.StatusNotFound, common.NewError("comments", errors.New("Invalid slug")))
		return
	}
	err = entityModel.getComments()
	if err != nil {
		c.JSON(http.StatusNotFound, common.NewError("comments", errors.New("Database error")))
		return
	}
	serializer := CommentsSerializer{c, entityModel.Comments}
	c.JSON(http.StatusOK, gin.H{"comments": serializer.Response()})
}
func TagList(c *gin.Context) {
	tagModels, err := getAllTags()
	if err != nil {
		c.JSON(http.StatusNotFound, common.NewError("entities", errors.New("Invalid param")))
		return
	}
	serializer := TagsSerializer{c, tagModels}
	c.JSON(http.StatusOK, gin.H{"tags": serializer.Response()})
}
