package entities

import (
	"github.com/gosimple/slug"
	"github.com/wangzitian0/golang-gin-starter-kit/common"
	"github.com/wangzitian0/golang-gin-starter-kit/users"
	"gopkg.in/gin-gonic/gin.v1"
)

type entityModelValidator struct {
	entity struct {
		Title       string   `form:"title" json:"title" binding:"exists,min=4"`
		Description string   `form:"description" json:"description" binding:"max=2048"`
		Body        string   `form:"body" json:"body" binding:"max=2048"`
		Tags        []string `form:"tagList" json:"tagList"`
	} `json:"entity"`
	entityModel entityModel `json:"-"`
}

func NewentityModelValidator() entityModelValidator {
	return entityModelValidator{}
}

func NewentityModelValidatorFillWith(entityModel entityModel) entityModelValidator {
	entityModelValidator := NewentityModelValidator()
	entityModelValidator.entity.Title = entityModel.Title
	entityModelValidator.entity.Description = entityModel.Description
	entityModelValidator.entity.Body = entityModel.Body
	for _, tagModel := range entityModel.Tags {
		entityModelValidator.entity.Tags = append(entityModelValidator.entity.Tags, tagModel.Tag)
	}
	return entityModelValidator
}

func (s *entityModelValidator) Bind(c *gin.Context) error {
	myUserModel := c.MustGet("my_user_model").(users.UserModel)

	err := common.Bind(c, s)
	if err != nil {
		return err
	}
	s.entityModel.Slug = slug.Make(s.entity.Title)
	s.entityModel.Title = s.entity.Title
	s.entityModel.Description = s.entity.Description
	s.entityModel.Body = s.entity.Body
	s.entityModel.Author = GetentityUserModel(myUserModel)
	s.entityModel.setTags(s.entity.Tags)
	return nil
}

type CommentModelValidator struct {
	Comment struct {
		Body string `form:"body" json:"body" binding:"max=2048"`
	} `json:"comment"`
	commentModel CommentModel `json:"-"`
}

func NewCommentModelValidator() CommentModelValidator {
	return CommentModelValidator{}
}

func (s *CommentModelValidator) Bind(c *gin.Context) error {
	myUserModel := c.MustGet("my_user_model").(users.UserModel)

	err := common.Bind(c, s)
	if err != nil {
		return err
	}
	s.commentModel.Body = s.Comment.Body
	s.commentModel.Author = GetentityUserModel(myUserModel)
	return nil
}
