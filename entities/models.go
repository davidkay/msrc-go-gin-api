package entities

import (
	_ "fmt"
	"github.com/jinzhu/gorm"
	"github.com/wangzitian0/golang-gin-starter-kit/common"
	"github.com/wangzitian0/golang-gin-starter-kit/users"
	"strconv"
)

type entityModel struct {
	gorm.Model
	Slug        string `gorm:"unique_index"`
	Title       string
	Description string `gorm:"size:2048"`
	Body        string `gorm:"size:2048"`
	Author      entityUserModel
	AuthorID    uint
	Tags        []TagModel     `gorm:"many2many:entity_tags;"`
	Comments    []CommentModel `gorm:"ForeignKey:entityID"`
}

type entityUserModel struct {
	gorm.Model
	UserModel      users.UserModel
	UserModelID    uint
	entityModels  []entityModel  `gorm:"ForeignKey:AuthorID"`
	FavoriteModels []FavoriteModel `gorm:"ForeignKey:FavoriteByID"`
}

type FavoriteModel struct {
	gorm.Model
	Favorite     entityModel
	FavoriteID   uint
	FavoriteBy   entityUserModel
	FavoriteByID uint
}

type TagModel struct {
	gorm.Model
	Tag           string         `gorm:"unique_index"`
	entityModels []entityModel `gorm:"many2many:entity_tags;"`
}

type CommentModel struct {
	gorm.Model
	entity   entityModel
	entityID uint
	Author    entityUserModel
	AuthorID  uint
	Body      string `gorm:"size:2048"`
}

func GetentityUserModel(userModel users.UserModel) entityUserModel {
	var entityUserModel entityUserModel
	if userModel.ID == 0 {
		return entityUserModel
	}
	db := common.GetDB()
	db.Where(&entityUserModel{
		UserModelID: userModel.ID,
	}).FirstOrCreate(&entityUserModel)
	entityUserModel.UserModel = userModel
	return entityUserModel
}

func (entity entityModel) favoritesCount() uint {
	db := common.GetDB()
	var count uint
	db.Model(&FavoriteModel{}).Where(FavoriteModel{
		FavoriteID: entity.ID,
	}).Count(&count)
	return count
}

func (entity entityModel) isFavoriteBy(user entityUserModel) bool {
	db := common.GetDB()
	var favorite FavoriteModel
	db.Where(FavoriteModel{
		FavoriteID:   entity.ID,
		FavoriteByID: user.ID,
	}).First(&favorite)
	return favorite.ID != 0
}

func (entity entityModel) favoriteBy(user entityUserModel) error {
	db := common.GetDB()
	var favorite FavoriteModel
	err := db.FirstOrCreate(&favorite, &FavoriteModel{
		FavoriteID:   entity.ID,
		FavoriteByID: user.ID,
	}).Error
	return err
}

func (entity entityModel) unFavoriteBy(user entityUserModel) error {
	db := common.GetDB()
	err := db.Where(FavoriteModel{
		FavoriteID:   entity.ID,
		FavoriteByID: user.ID,
	}).Delete(FavoriteModel{}).Error
	return err
}

func SaveOne(data interface{}) error {
	db := common.GetDB()
	err := db.Save(data).Error
	return err
}

func FindOneentity(condition interface{}) (entityModel, error) {
	db := common.GetDB()
	var model entityModel
	tx := db.Begin()
	tx.Where(condition).First(&model)
	tx.Model(&model).Related(&model.Author, "Author")
	tx.Model(&model.Author).Related(&model.Author.UserModel)
	tx.Model(&model).Related(&model.Tags, "Tags")
	err := tx.Commit().Error
	return model, err
}

func (self *entityModel) getComments() error {
	db := common.GetDB()
	tx := db.Begin()
	tx.Model(self).Related(&self.Comments, "Comments")
	for i, _ := range self.Comments {
		tx.Model(&self.Comments[i]).Related(&self.Comments[i].Author, "Author")
		tx.Model(&self.Comments[i].Author).Related(&self.Comments[i].Author.UserModel)
	}
	err := tx.Commit().Error
	return err
}

func getAllTags() ([]TagModel, error) {
	db := common.GetDB()
	var models []TagModel
	err := db.Find(&models).Error
	return models, err
}

func FindManyentity(tag, author, limit, offset, favorited string) ([]entityModel, int, error) {
	db := common.GetDB()
	var models []entityModel
	var count int

	offset_int, err := strconv.Atoi(offset)
	if err != nil {
		offset_int = 0
	}

	limit_int, err := strconv.Atoi(limit)
	if err != nil {
		limit_int = 20
	}

	tx := db.Begin()
	if tag != "" {
		var tagModel TagModel
		tx.Where(TagModel{Tag: tag}).First(&tagModel)
		if tagModel.ID != 0 {
			tx.Model(&tagModel).Offset(offset_int).Limit(limit_int).Related(&models, "entityModels")
			count = tx.Model(&tagModel).Association("entityModels").Count()
		}
	} else if author != "" {
		var userModel users.UserModel
		tx.Where(users.UserModel{Username: author}).First(&userModel)
		entityUserModel := GetentityUserModel(userModel)

		if entityUserModel.ID != 0 {
			count = tx.Model(&entityUserModel).Association("entityModels").Count()
			tx.Model(&entityUserModel).Offset(offset_int).Limit(limit_int).Related(&models, "entityModels")
		}
	} else if favorited != "" {
		var userModel users.UserModel
		tx.Where(users.UserModel{Username: favorited}).First(&userModel)
		entityUserModel := GetentityUserModel(userModel)
		if entityUserModel.ID != 0 {
			var favoriteModels []FavoriteModel
			tx.Where(FavoriteModel{
				FavoriteByID: entityUserModel.ID,
			}).Offset(offset_int).Limit(limit_int).Find(&favoriteModels)

			count = tx.Model(&entityUserModel).Association("FavoriteModels").Count()
			for _, favorite := range favoriteModels {
				var model entityModel
				tx.Model(&favorite).Related(&model, "Favorite")
				models = append(models, model)
			}
		}
	} else {
		db.Model(&models).Count(&count)
		db.Offset(offset_int).Limit(limit_int).Find(&models)
	}

	for i, _ := range models {
		tx.Model(&models[i]).Related(&models[i].Author, "Author")
		tx.Model(&models[i].Author).Related(&models[i].Author.UserModel)
		tx.Model(&models[i]).Related(&models[i].Tags, "Tags")
	}
	err = tx.Commit().Error
	return models, count, err
}

func (self *entityUserModel) GetentityFeed(limit, offset string) ([]entityModel, int, error) {
	db := common.GetDB()
	var models []entityModel
	var count int

	offset_int, err := strconv.Atoi(offset)
	if err != nil {
		offset_int = 0
	}
	limit_int, err := strconv.Atoi(limit)
	if err != nil {
		limit_int = 20
	}

	tx := db.Begin()
	followings := self.UserModel.GetFollowings()
	var entityUserModels []uint
	for _, following := range followings {
		entityUserModel := GetentityUserModel(following)
		entityUserModels = append(entityUserModels, entityUserModel.ID)
	}

	tx.Where("author_id in (?)", entityUserModels).Order("updated_at desc").Offset(offset_int).Limit(limit_int).Find(&models)

	for i, _ := range models {
		tx.Model(&models[i]).Related(&models[i].Author, "Author")
		tx.Model(&models[i].Author).Related(&models[i].Author.UserModel)
		tx.Model(&models[i]).Related(&models[i].Tags, "Tags")
	}
	err = tx.Commit().Error
	return models, count, err
}

func (model *entityModel) setTags(tags []string) error {
	db := common.GetDB()
	var tagList []TagModel
	for _, tag := range tags {
		var tagModel TagModel
		err := db.FirstOrCreate(&tagModel, TagModel{Tag: tag}).Error
		if err != nil {
			return err
		}
		tagList = append(tagList, tagModel)
	}
	model.Tags = tagList
	return nil
}

func (model *entityModel) Update(data interface{}) error {
	db := common.GetDB()
	err := db.Model(model).Update(data).Error
	return err
}

func DeleteentityModel(condition interface{}) error {
	db := common.GetDB()
	err := db.Where(condition).Delete(entityModel{}).Error
	return err
}

func DeleteCommentModel(condition interface{}) error {
	db := common.GetDB()
	err := db.Where(condition).Delete(CommentModel{}).Error
	return err
}
